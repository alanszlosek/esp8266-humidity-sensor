#include <DHT.h>


/*
 * To upload, hold both buttons for 3+ seconds, red light will stay on, then click compile and upload
 */

#include <ESP8266WiFi.h>

#include <FS.h>

#include <WiFiUdp.h>

// Create the UDP object and configure the StatsD client.
WiFiUDP udp;  // or EthernetUDP, as appropriate.


#define DHTPIN 2     // what digital pin we're connected to
#define DHTTYPE DHT22

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  dht.begin();

  WiFi.begin("your-wifi-essid", "your wifi password");

}

void loop() {

  WiFi.setAutoReconnect(true);

  // Wait a minute between measurements
  delay(60000);

  // Reconnect to wifi if it disconnected
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Re-connecting...");
  }
  Serial.println("Connected!");

  

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  //float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  char message[32];
  char temp1[6], temp2[6];
  /* 4 is mininum width, 2 is precision; float value is copied onto str_temp*/
  dtostrf(h, 1, 2, temp1);
  dtostrf(f, 1, 2, temp2);
  sprintf(message, "%s%%%sF\n", temp1, temp2);
  

  udp.beginPacket("192.168.1.1", 8192);
  udp.write(message, strlen(message));
  udp.endPacket();

  /*
  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);
  */

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  //Serial.print(t);
  //Serial.print(" *C ");
  Serial.print(f);
  Serial.print(" *F\t");
  /*
  Serial.print("Heat index: ");
  Serial.print(hic);
  Serial.print(" *C ");
  Serial.print(hif);
  Serial.println(" *F");
  */

}
